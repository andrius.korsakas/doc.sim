<h2>Deliverable</h2>
A software designed to efficiently identifying textually similar documents in a large collection of large documents.<br>
Based on: "Mining of Massive Datasets" Jure Leskovec, Anand Rajaraman, Jeff Ullman

Chapter 3  Finding similar items<br><br>
The goal of this project is to develop a program, with its own collection of libraries, able to find textually similar documents in a provided corpus. Implementation will focus on performance and object orientated design, as a library/program of this sort is a valid candidate for future extensions and updates.<br>
Shingling of a text, is one of the most effective ways to identify lexically similar documents. The program will parse a corpus of specified files to a document-shingles pairs. Sets of shingles are large.

Development documentationt at [Wiki pages...](https://gitlab.com/andrius.korsakas/doc.sim/wikis/Project-plan)