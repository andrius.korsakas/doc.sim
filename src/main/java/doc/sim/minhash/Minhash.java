package doc.sim.minhash;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Minhash {

	public List<Integer> randomRows(int rows) {
		Random rnd = new Random();
		List<Integer> rndRows = new ArrayList<>();
		for(int i = 0; i < 200; i++) {
			int row = rnd.nextInt(rows);
			if(rndRows.contains(row)) {
				row = rnd.nextInt(rows);
				while(rndRows.contains(row)) {
					row = rnd.nextInt(rows);
				}
			}
			rndRows.add(row);
		}
		return rndRows;
	}
}
