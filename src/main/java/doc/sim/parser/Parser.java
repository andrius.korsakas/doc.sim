package doc.sim.parser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
/**
 * Parser class provides file parsing implementation.
 * 
 * @author ak
 */
public class Parser {
	//A regex pattern to match
	Pattern p = Pattern.compile("[^A-Za-z0-9 ]");
	
	/**
	 * File parsing method.
	 * Initializes BufferedReader object to file input stream, reads file line by line.
	 * Each line is processed as follows:
	 * -line split into words
	 * -each word is hashed and added to list
	 * 
	 * @param filePath the path of file to be processed.
	 * @return a list of hashed words.
	 */
	public List<String> parse(String filePath){
		List<String>hashedWords = new ArrayList<>(); 
		try { 
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
			br.lines().forEach(s -> { 
				if(s.length() > 0) { 
					String [] line = p.matcher(s.toLowerCase()).replaceAll("").replaceAll("( )+", " ").trim().split(" ");
					hashedWords.addAll(Arrays.asList(line));
				}
			}); 
			
			br.close(); 
		}catch (Exception e) {
			e.printStackTrace(); 
		}
		return hashedWords;
	}
	
	//This implementation proved to be slower than the one above,
	//		try { 
	//			List<String> asd = Files.lines(f.toPath()).flatMap(line ->
	//				Stream.of(p.matcher(line.toLowerCase()).replaceAll("").replaceAll("( )+",
	//					" ").trim().split(" "))).filter(s -> s.length() !=
	//						0).collect(Collectors.toList()); 
	//
	//		} catch (IOException e) { // TODO Auto-generated catch block
	//			e.printStackTrace(); }

	
}
