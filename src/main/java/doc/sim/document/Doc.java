package doc.sim.document;

import java.util.Set;
import java.util.TreeSet;
/**
 * This class represents a processed document.
 * Holds document name and shingles set.
 * 
 * @author ak
 *
 */
public class Doc {
	private String filename;
	private Set<Integer> shingles = new TreeSet<>();

	public Doc(String filename, Set<Integer> shingles) {
		super();
		this.filename = filename;
		this.shingles = shingles;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Set<Integer> getShingles() {
		return shingles;
	}

	public void setShingles(Set<Integer> shingles) {
		this.shingles = shingles;
	}
}
