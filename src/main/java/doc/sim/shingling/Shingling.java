package doc.sim.shingling;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Shingling {
	
	public Set<Integer> toShingles(List<String> docWordsList) {
		Set<Integer> shingles = new TreeSet<>();
		
		int index = 0;
	    while(index < docWordsList.size() - 3) {
			String shingle = docWordsList.get(index) +" "+ docWordsList.get(index + 1) +" "+ docWordsList.get(index + 2) +" "+ docWordsList.get(index + 3);
			shingles.add(shingle.hashCode());
			index++;
	    }
		return (TreeSet<Integer>) shingles;
	}
}
