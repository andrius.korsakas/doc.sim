package doc.sim;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.BeforeClass;
import org.junit.Test;

import doc.sim.document.Doc;
import doc.sim.parser.Parser;
import doc.sim.shingling.Shingling;

public class DocSimTests {
	
	private Set<Integer> universalShingleSet = new TreeSet<Integer>();
	private File[] files;
	private Map<String, Doc> docShignelMap = new TreeMap<>();

	private void loadFiles(String dir) {
		files = new File(dir).listFiles(new FilenameFilter() {
			public boolean accept(File current, String name) {
				return new File(current, name).isFile();
			}
		});
	}

	private void docsToShingles() {
		Parser parser = new Parser();

		for(File f : files) {
			Set<Integer> shingleSet = new Shingling().toShingles(parser.parse(f.getPath()));
			
			Doc doc = new Doc(f.getName(), shingleSet);
			
			universalShingleSet.addAll(shingleSet);
			docShignelMap.put(f.getName(), doc);
		}
	}
		
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testDocsToShinglesDocCount() {
		loadFiles("src/test/java/largeTestFiles");
		docsToShingles();
		
		int actual = docShignelMap.size();
		
		int expected = 2;
		
		assertEquals("DocSim doc count fail", expected, actual);
	}
	
	@Test
	public void testDocsToShinglesUnhiShinglesCount() {
		loadFiles("src/test/java/largeTestFiles");
		docsToShingles();
		
		int actual = universalShingleSet.size();
		
		int expected = 11394;
		assertEquals("DocSim univesal shingle count fail", expected, actual);
	}

}
