package doc.sim;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

import doc.sim.parser.Parser;

public class ParserTests {
	private static List<String> actual;
	
	@BeforeClass
	public static void parseTestFile() {
		Parser parser = new Parser();
		actual = parser.parse("src/test/java/test");
	}
	
	@Test
	public void parseWordCountTest() {
		int expected = 5;
		assertEquals("Word count test fail", expected, actual.size());
	}
	
	@Test
	public void parseHashValuesTest() {
		List<String> expected = new ArrayList<>();
		String[] values = {"he", "was", "an", "old", "man"};
		expected.addAll(Arrays.asList(values));
		
		assertEquals("Values test failed", expected, actual);
	}

}
