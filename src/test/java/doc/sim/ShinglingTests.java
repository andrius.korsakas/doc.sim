package doc.sim;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.BeforeClass;
import org.junit.Test;

import doc.sim.parser.Parser;
import doc.sim.shingling.Shingling;

public class ShinglingTests {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void toShinglesShingleCountTest() {
		Integer sh0 = 3325 + 117481 + 3117 + 110119;
		Integer sh1 = 117481 + 3117 + 110119 + 107866;
		
		sh0 = sh0.hashCode();
		sh1 = sh1.hashCode();
		Set<Integer> testShingles = new TreeSet<>();
		testShingles.add(sh0);
		testShingles.add(sh1);
		
		int expected = testShingles.size();
		
		Parser parser = new Parser();
		List<String> testWordList = parser.parse("src/test/java/test");
		
		Set<Integer> actual = new Shingling().toShingles(testWordList);
		
		assertEquals("Shingles count test failed", expected , actual.size());
	}
	
	@Test
	public void toShinglesShingleValueTest() {
		Integer sh0 = 965466062;
		Integer sh1 = 1580429445;
		
		Set<Integer> expected = new TreeSet<>();
		expected.add(sh0);
		expected.add(sh1);
		
		Parser parser = new Parser();
		List<String> testWordList = parser.parse("src/test/java/test");
		TreeSet<Integer> actual = (TreeSet<Integer>) new Shingling().toShingles( testWordList);
		
		assertEquals("Shingles value test failed", expected, actual);
	}


}
