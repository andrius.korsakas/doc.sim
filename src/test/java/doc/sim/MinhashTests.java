package doc.sim;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import doc.sim.minhash.Minhash;

public class MinhashTests {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testRandomRows() {
		Minhash mintest = new Minhash();
		
		List<Integer> actual = mintest.randomRows(11393);
		actual.forEach(System.out::println);
		assertEquals("Random row list size fail", 200, actual.size());
	}

}
